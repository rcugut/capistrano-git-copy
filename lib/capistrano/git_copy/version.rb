# frozen_string_literal: true

# Capistrano
module Capistrano
  # GitCopy
  module GitCopy
    # gem version
    VERSION = '1.5.4'
  end
end
